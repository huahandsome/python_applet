import ctypes
from ctypes import cdll

'''
PROTOTYPE of test.c

float* test(float input1, int input2, int input3)
'''

input1 = ctypes.c_float(1.2000)
input2 = ctypes.c_int(10)
input3 = ctypes.c_int(20)

test_lib = cdll.LoadLibrary("./test1.so")

test_lib.restype=ctypes.POINTER(ctypes.c_float)

output = test_lib.test(input1, input2, input3)

print output
