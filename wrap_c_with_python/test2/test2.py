import ctypes
from ctypes import cdll

'''
PROTOTYPE of test.c

void test(float* p_ret, float* input1, int input2, int input3)
'''

input1 = ctypes.c_float(1.2000)
input2 = ctypes.c_int(10)
input3 = ctypes.c_int(20)

output = ctypes.c_float()

test_lib = cdll.LoadLibrary("./test2.so")

test_lib.test(ctypes.byref(output), ctypes.byref(input1), input2, input3)
print output
print output.value
