python2.7 download_numpy_i.py
swig -c++ -python inplace.i
#
# https://stackoverflow.com/questions/6045809/link-error-undefined-reference-to-gxx-personality-v0-and-g
# make sure put -lstdc++ at the end of the command
#
g++ -fPIC -o inplace.o -c inplace.cpp -lstdc++
g++ -fPIC -o inplace_wrap.o -c inplace_wrap.cxx -I/usr/include/python2.7 -lstdc++
g++ -fPIC -shared inplace.o inplace_wrap.o -o _inplace.so -lstdc++

