import ctypes
from ctypes import cdll
import numpy as np
'''
PROTOTYPE of test.c

void test(float* p_ret, float* input1, int input2, int input3)
'''

arr = np.array([1, 2])

input1 = (ctypes.c_float * len(arr))(*arr)
input2 = ctypes.c_int(10)
input3 = ctypes.c_int(20)

output = ctypes.c_float()

test_lib = cdll.LoadLibrary("./test3.so")

test_lib.test(ctypes.byref(output), input1, input2, input3)
print output
print output.value
